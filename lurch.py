#!/usr/bin/env python3

from modules.configuration import Config
from modules.connection import Connection
from modules.control import Control

from plugins.echo import Echo
from plugins.urban import Urban

def main():
    cfg = Config("lurch.cfg")
    conn = Connection(cfg.server, cfg.port)
    control = Control(cfg, conn)

    __setup_plugins(control, cfg, conn)

    control.run()

def __setup_plugins(control, cfg, conn):
    echo = Echo(cfg, conn)
    urban = Urban(cfg, conn)
    control.install(echo)
    control.install(urban)

if __name__ == "__main__":
    main()
