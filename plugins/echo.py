from modules.plugin import Plugin

class Echo(Plugin):

    def __init__(self, cfg, conn):
        super(Echo, self).__init__(cfg, conn, "echo", False)

    def parse(self, message):
        self.reply("You said: {0}".format(message))

    def tick(self, message):
        pass
