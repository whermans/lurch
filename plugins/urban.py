from modules.plugin import Plugin
from urllib.request import urlopen, HTTPError
import json

class Urban(Plugin):

    def __init__(self, cfg, conn):
        super(Urban, self).__init__(cfg, conn, "ud", False)

    def parse(self, message):
        query = message
        reply = "Sorry, no idea what \"{0}\" means".format(message)

        split = message.split()
        if len(split) > 1:
            query = '_'.join(split)
        try:
            answer = self.__lookup(query)
            if answer != "":
                reply = "{0}: {1}".format(message, answer)
        except ValueError:
            pass
        except HTTPError:
            pass

        self.reply(reply)


    def __lookup(self, query):
        answer = ""

        query = ''.join(c for c in query if c.isalnum())
        url = "http://api.urbandictionary.com/v0/define?term={0}".format(query)
        try:
            reply = json.loads(self.__get(url))
            if len(reply) > 0:
                answer = reply["list"][0]["definition"]
                answer = answer.replace('\r', ' ')
                answer = answer.replace('\n', ' ')
                answer = answer[:400] + "..."
        except ValueError:
            pass
        except IndexError:
            pass

        return answer

    def __get(self, url):
        response = urlopen(url).read()
        return str(response, "UTF-8")

    def tick(self, message):
        pass
