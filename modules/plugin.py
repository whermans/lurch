from modules.configuration import Config
from modules.connection import Connection

class Plugin:

    cfg = None
    conn = None

    timed = False
    key = ""

    def __init__(self, cfg, conn, key, timed=False):
        self.cfg = cfg
        self.conn = conn
        self.key = key
        self.timed = timed

    def parse(self, message):
        raise NotImplementedError

    def tick(self):
        raise NotImplementedError

    def reply(self, message):
        reply = "PRIVMSG {0} :{1}".format(self.cfg.channel, message)
        self.conn.send(reply)
