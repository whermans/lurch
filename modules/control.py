import threading

class Control:

    conn = cfg = None
    user_msg= "USER {0} 0 * {1}"
    pass_msg = "PASS {0}"
    nick_msg = "NICK {0}"
    join_msg = "JOIN {0}"

    plugins = {}
    timed = {}

    def __init__(self, cfg, conn):
        self.cfg = cfg
        self.conn = conn
        self.__register()

    def __register(self):
        self.conn.send(self.user_msg.format(self.cfg.nick, self.cfg.real_name))
        self.conn.send(self.pass_msg.format(self.cfg.password))
        self.conn.send(self.nick_msg.format(self.cfg.nick))

        while 1:
            recv = self.conn.receive()
            if "372" in recv or "422" in recv:
                break
        self.conn.send(self.join_msg.format(self.cfg.channel))

    def install(self, plugin):
        key = plugin.key
        if plugin.timed:
            self.timed[key] = plugin
        else:
            self.plugins[key] = plugin
        print("Installed plugin {0}".format(plugin.key))

    def run(self):
        threading.Timer(0.5, self.run).start()
        self.__dispatch()

    def __dispatch(self):
        for t in self.timed.keys():
            self.timed[t].tick()
        message = self.__clean(self.conn.receive())
        for p in self.plugins.keys():
            fullkey = "!{0}".format(p)
            if message.startswith(fullkey):
                self.plugins[p].parse(message.split(fullkey + " ")[1])

    def __clean(self, message):
        parts = message.split("PRIVMSG {0} :".format(self.cfg.channel))
        if len(parts) > 1:
            return parts[1]
        return message
