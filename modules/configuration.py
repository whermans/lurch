import configparser

class Config:

    data = {}

    server = ""
    port = 6667

    nick = ""
    password = ""
    real_name = ""

    channel = ""

    def __init__(self, filename):
        self.data = self.__load(filename)
        self.__read_general()

    def __read_general(self):
        section = "general"
        self.server = self.data[section]["server"]
        self.port = int(self.data[section]["port"])
        self.nick = self.data[section]["nick"]
        self.password = self.data[section]["password"]
        self.real_name = self.data[section]["real_name"]
        channel = self.data[section]["channel"]
        self.channel = "#{0}".format(channel)

    def __load(self, filename):
        data = {}
        parser = configparser.ConfigParser()

        parser.read(filename)
        for section in parser.sections():
            section_data = {}
            for option in parser.options(section):
                try:
                    value = parser.get(section, option)
                    if value != -1:
                        section_data[option] = value
                    else:
                        section_data[option] = None
                except:
                    section_data[option] = None
            data[section] = section_data
        return data

    def get(self, section, option):
        return self.data[section][option]
