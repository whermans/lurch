import socket

class Connection:

    server = ""
    port = None
    socket = None

    def __init__(self, server, port):
        self.server = server
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Connecting to {0} on port {1}".format(server, port))
        self.__connect()

    def __connect(self):
        self.socket.connect((self.server, self.port))

    def disconnect(self):
        self.socket.shutdown(self.socket.SHUT_RDWR)
        self.socket.close()

    def send(self, message):
        print("|==>|\n{0}\n".format(message))
        data = bytes(message + "\n", "UTF-8")
        self.socket.send(data)

    def receive(self):
        data = self.socket.recv(2048)
        message = str(data, "UTF-8").strip("\r\n")
        self.__ping(message)
        print("|<==|\n{0}\n".format(message))
        return message

    def __ping(self, message):
        if message.startswith("PING"):
            pong = message.split(":")[1]
            self.send("PONG :{0}".format(pong))
